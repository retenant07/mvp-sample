package mvpsample.javra.com.mvpsample;

import java.util.List;

/**
 * Created by ritendra on 5/17/2016.
 */
public interface MainInteract {

    interface OnFinishedListener {
        void onFinished(List<String> items);
    }

    void findItems(OnFinishedListener listener);
}
