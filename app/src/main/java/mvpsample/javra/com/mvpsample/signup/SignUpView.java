package mvpsample.javra.com.mvpsample.signup;

/**
 * Created by ritendra on 5/18/2016.
 */
public interface SignUpView {

    void showProgress();

    void hideProgress();

    void goToMainActivity();

    void showMessage(String message);

}
