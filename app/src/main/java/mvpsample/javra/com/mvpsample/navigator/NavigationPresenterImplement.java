package mvpsample.javra.com.mvpsample.navigator;

import android.util.Log;

/**
 * Created by ritendra on 5/24/2016.
 */
public class NavigationPresenterImplement implements NavigationPresenter {

    private NavigationView navigationView;

    public NavigationPresenterImplement(NavigationView navigationView) {

        this.navigationView = navigationView;
    }

    @Override
    public void OnDrawerItemClick(long position) {

        if(navigationView != null){

            navigationView.openFragment(position);
            Log.d("clicked","yes");
        }

    }

    @Override
    public void OnGoToNext() {

        if(navigationView != null){

            navigationView.goToNext();
        }
    }
}
