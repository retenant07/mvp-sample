package mvpsample.javra.com.mvpsample.signup;

/**
 * Created by ritendra on 5/18/2016.
 */
public interface SignUpInteract {

    interface onFinishedSignUp{

        void onError(String message);

        void onSuccess();
    }

    void signUp(String name, String password, String confirmPassword, onFinishedSignUp listener);
}
