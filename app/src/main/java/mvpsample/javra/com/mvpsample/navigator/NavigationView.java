package mvpsample.javra.com.mvpsample.navigator;

/**
 * Created by ritendra on 5/24/2016.
 */
public interface NavigationView {

    void openFragment(long position);

    void goToNext();
}
