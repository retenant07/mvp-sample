package mvpsample.javra.com.mvpsample.login;

/**
 * Created by ritendra on 5/17/2016.
 */
public interface LoginPresenter {

    void validateCredentials(String userName, String Password);

    void goToSignUp();

    void onDestroy();
}
