package mvpsample.javra.com.mvpsample;

import android.os.Handler;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ritendra on 5/17/2016.
 */
public class MainInteractImplement implements MainInteract {

    @Override
    public void findItems(final OnFinishedListener listener) {

        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                listener.onFinished(createArrayList());
            }
        }, 2000);
    }

    private List<String> createArrayList() {
        return Arrays.asList(
                "Chiranjivi",
                "Manjul",
                "Rajan",
                "Ritendra",
                "Sahi",
                "Samrakchan",
                "Android",
                "IOS",
                "React Native",
                "Hybrid app"
        );
    }
}
