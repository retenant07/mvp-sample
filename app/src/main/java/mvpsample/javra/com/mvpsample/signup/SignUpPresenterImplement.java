package mvpsample.javra.com.mvpsample.signup;

/**
 * Created by ritendra on 5/18/2016.
 */
public class SignUpPresenterImplement implements SignUpPresenter, SignUpInteract.onFinishedSignUp{

    private SignUpView signUpView;
    private SignUpInteract signUpInteract;


    public SignUpPresenterImplement(SignUpView signUpView) {
        this.signUpView = signUpView;
        this.signUpInteract = new SignUpInteractImplement();
    }

    @Override
    public void validateTextFields(String name, String password, String confirmPassword) {

        if(signUpView != null){
            signUpView.showProgress();
        }
        signUpInteract.signUp(name, password,  confirmPassword, this);
    }

    @Override
    public void goToMainActivity() {

        if(signUpView!=null){
            signUpView.goToMainActivity();
        }
    }

    @Override
    public void onError(String message) {

        if(signUpView!=null){
            signUpView.hideProgress();
            signUpView.showMessage(message);
        }
    }

    @Override
    public void onSuccess() {

        signUpView.hideProgress();
        signUpView.goToMainActivity();
    }
}
