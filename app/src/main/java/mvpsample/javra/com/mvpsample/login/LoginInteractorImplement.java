package mvpsample.javra.com.mvpsample.login;


import android.os.Handler;
import android.text.TextUtils;

/**
 * Created by ritendra on 5/17/2016.
 */
public class LoginInteractorImplement implements LoginInteract {
    @Override
    public void login(final String username, final String password, final OnLoginFinishedListener listener) {

        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                boolean error = false;
                if (TextUtils.isEmpty(username) || !username.equalsIgnoreCase("r")){
                    listener.onUsernameError();
                    error = true;
                }
                if (TextUtils.isEmpty(password) || !password.equalsIgnoreCase("r")){
                    listener.onPasswordError();
                    error = true;
                }
                if (!error){
                    listener.onSuccess();
                }
            }
        }, 2000);

    }
}
