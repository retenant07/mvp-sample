package mvpsample.javra.com.mvpsample.signup;

/**
 * Created by ritendra on 5/18/2016.
 */
public interface SignUpPresenter {

    void validateTextFields(String name, String password, String confirmPassword);

    void goToMainActivity();
}
