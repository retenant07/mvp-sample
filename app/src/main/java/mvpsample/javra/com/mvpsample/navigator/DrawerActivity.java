package mvpsample.javra.com.mvpsample.navigator;

import android.content.res.Configuration;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import mvpsample.javra.com.mvpsample.R;
import mvpsample.javra.com.mvpsample.adapter.DrawerAdapter;
import mvpsample.javra.com.mvpsample.fragment.DashboardFragment;
import mvpsample.javra.com.mvpsample.model.DrawerItem;

public class DrawerActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener, DashboardFragment.OnFragmentInteractionListener {

    private static final String TAG = DrawerActivity.class.getSimpleName();
    private List<DrawerItem> mDrawerItems;
    private DrawerLayout mDrawerLayout;
    public ActionBarDrawerToggle mActionBarDrawerToggle;
    private ActionBar mActionBar;

    private RecyclerView mDrawerRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private DrawerAdapter mDrawerAdapter;

    private FragmentTransaction mFragmentTransaction;
    private FragmentManager mFragmentManager;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mDrawerRecyclerView = (RecyclerView)findViewById(R.id.left_drawer);

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            mActionBar = getSupportActionBar();
            mActionBar.setElevation(4);
        }

        mDrawerItems = new ArrayList<>();
        mLinearLayoutManager = new LinearLayoutManager(this);
        mDrawerRecyclerView.setLayoutManager(mLinearLayoutManager);
        //mDrawerAdapter = new DrawerAdapter(DrawerActivity.this,mDrawerItems,mDrawerItemClickListener);
        mDrawerAdapter = new DrawerAdapter(this, mDrawerItems, mDrawerItemClickListener);
        mDrawerRecyclerView.setAdapter(mDrawerAdapter);

        mFragmentManager = getSupportFragmentManager();
        mFragmentManager.addOnBackStackChangedListener(this);

        addDrawerItem();

        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.app_name, R.string.app_name){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
    }

    private DrawerItemClickListener mDrawerItemClickListener = new DrawerItemClickListener() {
        @Override
        public void onDrawerItemClickListener(int position) {
            Log.d("item clicked", mDrawerItems.get(position).toString());
            selectFragment(position);
        }
    };

    private void selectFragment(int index){
        mFragmentTransaction = mFragmentManager.beginTransaction();
        switch (index){
            case 1:
                mFragmentTransaction.replace(R.id.content_frame, new DashboardFragment().newInstance("a", "b"));
                break;
            case 2:
                break;
            default:

        }
        mFragmentTransaction.commit();
        mDrawerLayout.closeDrawers();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public interface DrawerItemClickListener{
        void onDrawerItemClickListener(int position);
    }

    @Override
    public void onBackStackChanged() {

        shouldDisplayHomeUp();
    }

    private void addDrawerItem(){
        mDrawerItems.add(new DrawerItem("Home", R.mipmap.ic_launcher, ""));
        mDrawerItems.add(new DrawerItem("Home", R.mipmap.ic_launcher, ""));
        mDrawerItems.add(new DrawerItem("Nagarik", R.mipmap.ic_launcher, ""));
    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount() > 0)
            getFragmentManager().popBackStack();
        else
            super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getSupportFragmentManager().popBackStack();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void shouldDisplayHomeUp(){
        //Enable Up button only  if there are entries in the back stack
        boolean canback = getSupportFragmentManager().getBackStackEntryCount()>0;
        mActionBar.setDisplayHomeAsUpEnabled(canback);
        mActionBar.setHomeButtonEnabled(canback);
        mActionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onSupportNavigateUp() {
        getSupportFragmentManager().popBackStack();
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mActionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mActionBarDrawerToggle.onConfigurationChanged(newConfig);
    }
}
