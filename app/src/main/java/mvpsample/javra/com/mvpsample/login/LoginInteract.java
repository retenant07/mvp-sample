package mvpsample.javra.com.mvpsample.login;

/**
 * Created by ritendra on 5/17/2016.
 */
public interface LoginInteract {

    interface OnLoginFinishedListener {
        void onUsernameError();

        void onPasswordError();

        void onSuccess();
    }

    void login(String username, String password, OnLoginFinishedListener listener);
}
