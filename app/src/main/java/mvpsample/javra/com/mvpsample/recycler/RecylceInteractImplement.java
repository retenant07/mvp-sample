package mvpsample.javra.com.mvpsample.recycler;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mvpsample.javra.com.mvpsample.json.RoparunObjectRequest;
import mvpsample.javra.com.mvpsample.json.VolleySingleton;
import mvpsample.javra.com.mvpsample.model.Photographer;

/**
 * Created by ritendra on 5/18/2016.
 */
public class RecylceInteractImplement implements RecycleInteract {

    private onFinishedLoading listener;
    private Context mContext;

    private List<Photographer>photographers;

    public RecylceInteractImplement(Context mContext, onFinishedLoading listener) {
        this.mContext = mContext;
        this.listener = listener;
    }

    @Override
    public void getDataFromServer() {

        Map<String, String> headers;

        headers = new HashMap<>();
        headers.put("x-auth-token", "836a921ba30d9ba28a5d06b9430ac93a3209810e9c9e6ec14553193041adc3903cce9dfb071eb067a1fa63f2d5bc412e14626fad304aaa63dbc82da30972e1c7");


        RoparunObjectRequest objectRequest = new RoparunObjectRequest("http://photoos.boondrive.com/api/users/photographer",null, collectionRequestSuccessListener, collectionRequestErrorListener, headers);
        objectRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton volleySingleton = VolleySingleton.getInstance(mContext);
        volleySingleton.addToRequestQueue(objectRequest);
    }

    Response.Listener<JSONObject> collectionRequestSuccessListener = new Response.Listener<JSONObject>() {

        @Override
        public void onResponse(JSONObject response) {
            Log.i("ako ho", " "+response);
               parsePhotographerList(response);
        }
    };

    Response.ErrorListener collectionRequestErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {

            listener.onFailure();
            }
    };

    public void parsePhotographerList(JSONObject response){

        try {

            if(response.has("success")) {

                photographers = new ArrayList<>();

                if (response.getBoolean("success")) {

                    if (response.has("data")) {
                        JSONArray jsonArray = response.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Photographer photographer = new Photographer();
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            photographer.setName(jsonObject.has("fullName") ? jsonObject.getString("fullName") : "");
                            photographer.setEmail(jsonObject.has("email") ? jsonObject.getString("email") : "");
                            photographer.setUserId(jsonObject.has("_id") ? jsonObject.getString("_id") : "");
                            photographer.setDesc(jsonObject.has("personal_bio_info") ? jsonObject.getString("personal_bio_info") : "");
                            photographer.setImageurl(jsonObject.has("profile_pic") ? jsonObject.getString("profile_pic") : "");
                            photographer.setWebsite(jsonObject.has("personal_web_url") ? jsonObject.getString("personal_web_url") : "");
                            photographer.setFbLink(jsonObject.has("personal_fburl") ? jsonObject.getString("personal_fburl") : "");
                            photographer.setTwitterLink(jsonObject.has("personal_twiturl") ? jsonObject.getString("personal_twiturl") : "");
                            photographers.add(photographer);

                        }

                        listener.onSuccess(photographers);
                    }
                } else {
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    interface onFinishedLoading{

        void onSuccess(List<Photographer> photographerList);

        void onFailure();
    }
}
