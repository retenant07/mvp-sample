package mvpsample.javra.com.mvpsample.recycler;

import java.util.List;

import mvpsample.javra.com.mvpsample.model.Photographer;

/**
 * Created by ritendra on 5/18/2016.
 */
public interface RecyclerViewMVP {

    void showProgress();

    void hideProgress();

    void showMessage();

    void removeItemList(int position);

    void loadData(List<Photographer> photographerList);

    interface adapterOperation{
        void notifyItemRemoved(int position);
        void notifyDataSetChanged();
    }

}
