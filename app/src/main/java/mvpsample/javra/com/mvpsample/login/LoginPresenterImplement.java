package mvpsample.javra.com.mvpsample.login;

/**
 * Created by ritendra on 5/17/2016.
 */
public class LoginPresenterImplement implements LoginPresenter, LoginInteract.OnLoginFinishedListener{

    private LoginView loginView;
    private LoginInteract loginInteract;

    public LoginPresenterImplement(LoginView loginView) {
        this.loginView = loginView;
        this.loginInteract= new LoginInteractorImplement();
    }

    @Override
    public void validateCredentials(String userName, String Password) {

        if(loginView != null){
            loginView.showProgress();
        }

        loginInteract.login(userName, Password, this);
    }

    @Override
    public void goToSignUp() {
        if(loginView != null){
            loginView.goToSignUpActivity();
        }
    }

    @Override
    public void onDestroy() {
        loginView = null;
    }

    @Override
    public void onUsernameError() {
        if(loginView != null){
            loginView.setUserNameError();
            loginView.hideProgress();
        }
    }

    @Override
    public void onPasswordError() {

        if(loginView != null){
            loginView.setPasswordError();
            loginView.hideProgress();
        }
    }

    @Override
    public void onSuccess() {
        if(loginView != null){
            loginView.goToMain();
        }
    }
}
