package mvpsample.javra.com.mvpsample.utility;

import com.squareup.otto.Bus;

/**
 * Created by ritendra on 5/18/2016.
 */
public class MyBus {

    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }
}