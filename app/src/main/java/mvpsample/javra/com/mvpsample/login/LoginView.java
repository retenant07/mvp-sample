package mvpsample.javra.com.mvpsample.login;

/**
 * Created by ritendra on 5/17/2016.
 */
public interface LoginView {

    void showProgress();

    void hideProgress();

    void setUserNameError();

    void setPasswordError();

    void goToSignUpActivity();

    void goToMain();

}
