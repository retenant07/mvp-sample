package mvpsample.javra.com.mvpsample.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import mvpsample.javra.com.mvpsample.MainActivity;
import mvpsample.javra.com.mvpsample.R;
import mvpsample.javra.com.mvpsample.navigator.DrawerActivity;
import mvpsample.javra.com.mvpsample.navigator.NavigationActivity;
import mvpsample.javra.com.mvpsample.signup.SignUpActivity;

public class LoginActivity extends AppCompatActivity implements LoginView, View.OnClickListener {

    private EditText userName;
    private EditText userPassword;
    private ProgressBar progressBar;
    private Button loginBtn;
    private TextView signUpBtn;

    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        userName = (EditText) findViewById(R.id.name);
        userPassword = (EditText) findViewById(R.id.password);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        loginBtn = (Button) findViewById(R.id.loginBtn);
        signUpBtn = (TextView) findViewById(R.id.signUp);

        loginBtn.setOnClickListener(this);

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loginPresenter.goToSignUp();
            }
        });

        loginPresenter  = new LoginPresenterImplement(this);
    }

    @Override
    public void showProgress() {

        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {

        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setUserNameError() {
        userName.setError("Error");
    }

    @Override
    public void setPasswordError() {
        userPassword.setError("Error");
    }

    @Override
    public void goToSignUpActivity() {
        Intent signUp = new Intent(this, SignUpActivity.class);
        startActivity(signUp);
        finish();
    }

    @Override
    public void goToMain() {
        startActivity(new Intent(this, NavigationActivity.class));
        finish();
    }

    @Override
    public void onClick(View v) {
        loginPresenter.validateCredentials(userName.getText().toString(), userPassword.getText().toString());
    }
}
