package mvpsample.javra.com.mvpsample.model;

/**
 * Created by samrakchan on 1/5/2015.
 */
public class DrawerItem {
    private String title;
    private int icon;
    private String notification;


    public void setTitle(String title) {
        this.title = title;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getNotification() {
        return notification;
    }
    public String getTitle() {
        return title;
    }

    public int getIcon() {
        return icon;
    }

    public DrawerItem(String title, int icon, String notification) {
        this.title = title;
        this.icon = icon;
        this.notification = notification;
    }
}
