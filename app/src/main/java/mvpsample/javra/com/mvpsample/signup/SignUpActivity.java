

package mvpsample.javra.com.mvpsample.signup;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import mvpsample.javra.com.mvpsample.R;
import mvpsample.javra.com.mvpsample.recycler.RecyclerActivity;

public class SignUpActivity extends AppCompatActivity implements SignUpView, View.OnClickListener {


    private SignUpPresenter signUpPresenter;

    private EditText textName;
    private EditText textPassword;
    private EditText textConfirmPassword;
    private Button signUp;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        signUpPresenter = new SignUpPresenterImplement(this);

        textName = (EditText) findViewById(R.id.name);
        textPassword = (EditText) findViewById(R.id.password);
        textConfirmPassword = (EditText) findViewById(R.id.confirmPassword);
        signUp = (Button) findViewById(R.id.signUpBtn);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        signUp.setOnClickListener(this);
    }

    @Override
    public void showProgress() {

        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {

        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void goToMainActivity() {

        Intent goTo = new Intent(this, RecyclerActivity.class);
        startActivity(goTo);
    }

    @Override
    public void showMessage(String message) {

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {

        signUpPresenter.validateTextFields(textName.getText().toString(), textPassword.getText().toString(), textConfirmPassword.getText().toString());
    }
}
