package mvpsample.javra.com.mvpsample.recycler;

import java.util.List;

import mvpsample.javra.com.mvpsample.model.Photographer;

/**
 * Created by ritendra on 5/18/2016.
 */
public interface RecyclePresenter {

    void onResume();

    void onClickButton(int position);

    void removeItem(int position);

}
