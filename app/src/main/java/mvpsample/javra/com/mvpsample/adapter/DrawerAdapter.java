package mvpsample.javra.com.mvpsample.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import mvpsample.javra.com.mvpsample.R;
import mvpsample.javra.com.mvpsample.model.DrawerItem;
import mvpsample.javra.com.mvpsample.navigator.DrawerActivity;

/**
 * Created by ritendra on 1/22/2015.
 */
public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.ViewHolder>{
    private static final String TAG = DrawerAdapter.class.getSimpleName();
    private static final int ITEM_VIEW =0;
    private static final int COVER_VIEW=1;
    private static final int SEARCH_VIEW=2;
    private List<DrawerItem> mDrawerItems;
    private Context mContext;
    private DrawerActivity.DrawerItemClickListener mDrawerItemClickListener;

    public DrawerAdapter(Context context, List<DrawerItem> drawerItems, DrawerActivity.DrawerItemClickListener drawerItemClickListener){
        this.mDrawerItems = drawerItems;
        this.mContext = context;
        this.mDrawerItemClickListener = drawerItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=null;
        if(viewType==ITEM_VIEW){
            view =  LayoutInflater.from(parent.getContext()).inflate(R.layout.drawer_items, parent, false);
        }else if(viewType==COVER_VIEW){
            view =  LayoutInflater.from(parent.getContext()).inflate(R.layout.drawer_item_cover, parent, false);
        }
        ViewHolder vh = new ViewHolder(view, viewType);

        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if(position==0){

        }else{
            DrawerItem drawerItem = mDrawerItems.get(position);
            holder.titleTv.setText(drawerItem.getTitle());
            holder.iconIv.setImageResource(drawerItem.getIcon());

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mDrawerItemClickListener.onDrawerItemClickListener(position);
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0){
            return COVER_VIEW;
        }
        return ITEM_VIEW;
    }

    @Override
    public int getItemCount() {
        return mDrawerItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView titleTv;
        private ImageView iconIv;
        private View view;

        public ViewHolder(View itemView, int itemType) {
            super(itemView);
            view = itemView;
            if(itemType == ITEM_VIEW){
                iconIv = (ImageView)itemView.findViewById(R.id.drawer_item_icon);
                titleTv = (TextView)itemView.findViewById(R.id.drawer_title_text);
            }
        }
    }
}
