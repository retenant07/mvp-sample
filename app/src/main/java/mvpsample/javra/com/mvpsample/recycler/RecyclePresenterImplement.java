package mvpsample.javra.com.mvpsample.recycler;

import android.content.Context;

import java.util.List;

import mvpsample.javra.com.mvpsample.model.Photographer;

/**
 * Created by ritendra on 5/18/2016.
 */
public class RecyclePresenterImplement implements RecyclePresenter, RecylceInteractImplement.onFinishedLoading{

    private RecyclerViewMVP recyclerViewMVP;
    private RecycleInteract recycleInteract;
    private Context mContext;

    public RecyclePresenterImplement(RecyclerViewMVP recyclerViewMVP) {
        this.recyclerViewMVP = recyclerViewMVP;
        mContext = (Context)recyclerViewMVP;
        this.recycleInteract = new RecylceInteractImplement(mContext, this);
    }

    @Override
    public void onResume() {

        if(recyclerViewMVP!=null){
            recyclerViewMVP.showProgress();
        }

        recycleInteract.getDataFromServer();

    }

    @Override
    public void onClickButton(int position) {
        if(recyclerViewMVP!=null){
            recyclerViewMVP.showMessage();
        }
    }

    @Override
    public void removeItem(int position) {

        if(recyclerViewMVP!=null){
            recyclerViewMVP.removeItemList(position);
        }
    }


    @Override
    public void onSuccess(List<Photographer>photographerList) {

        recyclerViewMVP.hideProgress();
        recyclerViewMVP.loadData(photographerList);
    }

    @Override
    public void onFailure() {

        recyclerViewMVP.hideProgress();
        recyclerViewMVP.showMessage();

    }
}
