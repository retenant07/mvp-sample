package mvpsample.javra.com.mvpsample;

/**
 * Created by ritendra on 5/17/2016.
 */
public interface MainPresenter {

    void onItemClicked(int position);

    void onDestroy();

    void onStart();
}
