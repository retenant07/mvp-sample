package mvpsample.javra.com.mvpsample.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import mvpsample.javra.com.mvpsample.R;
import mvpsample.javra.com.mvpsample.model.Photographer;
import mvpsample.javra.com.mvpsample.recycler.RecyclePresenter;

/**
 * Created by ritendra on 5/18/2016.
 */
public class PhotographerAdapter extends RecyclerView.Adapter<PhotographerAdapter.ViewHolder> {

    private Context context;
    private RecyclePresenter recyclePresenter;
    private List<Photographer> photographerList;

    public PhotographerAdapter(Context context, List<Photographer> photographerList, RecyclePresenter recyclePresenter) {
        this.context = context;
        this.photographerList = photographerList;
        this.recyclePresenter = recyclePresenter;
    }

    @Override
    public PhotographerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_photographer, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PhotographerAdapter.ViewHolder holder, final int position) {

        holder.photographerName.setText(photographerList.get(position).getName());
        holder.photographerDesc.setText(photographerList.get(position).getDesc());

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //recyclePresenter.onClickButton(position);
                recyclePresenter.removeItem(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return photographerList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView photographerName;
        TextView photographerDesc;
        RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            photographerName = (TextView) itemView.findViewById(R.id.photographerName);
            photographerDesc = (TextView) itemView.findViewById(R.id.photographerDesc);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.main_layout);
        }
    }

    public void removeItem(int position) {
        photographerList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, photographerList.size());
    }

}