package mvpsample.javra.com.mvpsample;

import java.util.List;

/**
 * Created by ritendra on 5/17/2016.
 */
public interface MainView {

    void showProgress();

    void hideProgress();

    void showMessage(String message);

    void setItems(List<String> items);

}
