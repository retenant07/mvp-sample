package mvpsample.javra.com.mvpsample.signup;

import android.os.Handler;
import android.text.TextUtils;

/**
 * Created by ritendra on 5/18/2016.
 */
public class SignUpInteractImplement implements SignUpInteract {

    @Override
    public void signUp(final String name, final String password, final String confirmPassword, final onFinishedSignUp listener) {

        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                boolean error = false;
                if (TextUtils.isEmpty(name)){
                    listener.onError("Name field is empty");
                    error = true;
                }
                if (TextUtils.isEmpty(password)){
                    listener.onError("Password field is empty");
                    error = true;
                }

                if (TextUtils.isEmpty(confirmPassword)){
                    listener.onError("Password field is empty");
                    error = true;
                }

                if(!(password.equals(confirmPassword))){
                    listener.onError("Password does not match");
                    error = true;
                }

                if (!error){
                    listener.onSuccess();
                }

            }
        }, 2000);
    }
}
