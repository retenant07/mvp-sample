package mvpsample.javra.com.mvpsample.recycler;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import mvpsample.javra.com.mvpsample.R;
import mvpsample.javra.com.mvpsample.adapter.PhotographerAdapter;
import mvpsample.javra.com.mvpsample.model.Photographer;

public class RecyclerActivity extends AppCompatActivity implements RecyclerViewMVP {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private RecyclePresenter recyclePresenter;
    private PhotographerAdapter photographerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_activitiy);

        recyclePresenter = new RecyclePresenterImplement(this);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        setUpRecyclerView();

    }

    @Override
    public void onResume(){
        super.onResume();
        recyclePresenter.onResume();
    }


    public void setUpRecyclerView(){

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void showProgress() {

        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {

        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showMessage() {

        Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void removeItemList(int position) {

        photographerAdapter.removeItem(position);
    }

    @Override
    public void loadData(List<Photographer> photographerList) {

        photographerAdapter = new PhotographerAdapter(this, photographerList, recyclePresenter);
        recyclerView.setAdapter(photographerAdapter);
    }
}
