package mvpsample.javra.com.mvpsample;

import java.util.List;

/**
 * Created by ritendra on 5/17/2016.
 */
public class MainPresenterImplement implements MainPresenter, MainInteract.OnFinishedListener {

    private MainView mainView;
    private MainInteract mainInteract;

    public MainPresenterImplement(MainView mainView) {
        this.mainView = mainView;
        this.mainInteract = new MainInteractImplement();
    }

    @Override
    public void onItemClicked(int position) {
        if(mainView != null){
            mainView.showMessage(String.format("Position %d clicked", position + 1));
        }
    }

    @Override
    public void onDestroy() {

        mainView = null;
    }

    @Override
    public void onStart() {

        if(mainView != null){
            mainView.showProgress();
        }

        mainInteract.findItems(this);
    }

    @Override
    public void onFinished(List<String> items) {

        if(mainView != null){
            mainView.setItems(items);
            mainView.hideProgress();
        }
    }
}
